#!/usr/bin/env python
class World():

    phrase = "Hi world :)"

    def hello(self):
        print self.phrase


if __name__ == "__main__":
    w = World()
    w.hello()

